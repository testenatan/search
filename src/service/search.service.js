"use strict";

const algoliasearch = require('algoliasearch');
const client = algoliasearch(process.env.ALGOLIA_APP_ID, process.env.ALGOLIA_API_KEY)
const paperBotIndex = client.initIndex('paperbot');

const index = (document, cb) => paperBotIndex.addObject(document, cb);

const search = query => paperBotIndex.search(query);

module.exports = { index, search };
