"use strict";

const searchService = require("../service/search.service");

const handle = (event, ctx, cb) => {
  const message = JSON.parse(event.Records[0].body);
  if (!message.title || !message.content) {
    return cb(null, {
      statusCode: 400
    });
  }
  searchService.index(message, (err, content) => {
    if (err) {
      return cb(err);
    }
    cb(null, content);
  });
};

module.exports = { handle };
