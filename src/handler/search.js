"use strict";

const searchService = require("../service/search.service");

const handle = event => {
  const { query } = event.queryStringParameters || {};

  if (!query) {
    return _response(400, { error: "Query is required" });
  }

  return searchService
    .search(query)
    .then(res => _response(200, res.hits))
    .catch(err => _response(500, err));
};

const _response = (status, responseBody) => {
  const response = {
    statusCode: status,
    headers: {
      "Access-Control-Allow-Origin": "*"
    }
  };

  if (responseBody) response.body = JSON.stringify(responseBody);

  return Promise.resolve(response);
};

module.exports = { handle };
