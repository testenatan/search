Search API

## Quick Start

1. **Install via npm:**

```
npm install -g serverless
```

```
npm i
```

2. **Run locally:**

```
npm run dev
```

3. **Test**

```
npm tst
```

4. **Deploy**

```
npm run deploy
```
