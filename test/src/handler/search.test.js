"use strict";

const should = require("chai").should();
const proxyquire = require("proxyquire");
const event = require("../../mocks/event");
const handler = require("../../../src/handler/search");

const successHandler = proxyquire("../../../src/handler/search", {
  "../service/search.service": {
    search: q => Promise.resolve({ hits: [] })
  }
});

const errorHandler = proxyquire("../../../src/handler/search", {
  "../service/search.service": {
    search: q => Promise.reject()
  }
});

describe("Search Handler", () => {
  it("Should return 400 for invalid query param", () => {
    return handler.handle({}).then(res => {
      should.equal(res.statusCode, 400);
    });
  });

  it("Should return 200 for valid query", () => {
    return successHandler
      .handle({ queryStringParameters: { query: "lorem" } })
      .then(res => {
        should.equal(res.statusCode, 200);
      });
  });

  it("Should return 500 for algolia search error", () => {
    return errorHandler
      .handle({ queryStringParameters: { query: "lorem" } })
      .then(res => {
        should.equal(res.statusCode, 500);
      });
  });
});
