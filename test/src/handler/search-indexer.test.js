"use strict";

const should = require("chai").should();
const proxyquire = require("proxyquire");
const event = require("../../mocks/event");
const handler = require("../../../src/handler/search-indexer");

const successHandler = proxyquire("../../../src/handler/search-indexer", {
  "../service/search.service": {
    index: (document, cb) => cb(null, {})
  }
});

const errorHandler = proxyquire("../../../src/handler/search-indexer", {
  "../service/search.service": {
    index: (document, cb) => {
      return cb({ statusCode: 400 });
    }
  }
});

describe("Search Indexer Handler", () => {
  it("Should index event", () => {
    successHandler.handle(event, null, (err, res) => {
      should.exist(res);
    });
  });

  it("Should throw error from indexing", () => {
    errorHandler.handle(event, null, err => {
      should.exist(err);
    });
  });
});
